import os
import csv
import openpyxl

active_book = openpyxl.load_workbook(filename='excel/Тарифы по прокату - 8.xlsx')   # открываем excel файл
sheetnames = active_book.sheetnames                                        # сохраняем список листов
active_sheet = active_book[sheetnames[0]]              # меняем лист на первый

new_book = openpyxl.Workbook()
new_sheet = new_book.active
with open('csv_test.csv', 'w') as f:
    writer = csv.writer(f)
    for row in active_sheet.iter_rows(min_row=1, min_col=1, max_row=170, max_col=4):
        line = []
        for cell in row:
            line.append(cell.value[:])
        writer.writerow(line)

new_book.save(active_sheet.title+'.xlsx')
active_book.close()