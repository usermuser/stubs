import pandas as pd
import csv
import os

os.chdir('..')

clean_file = 'ПЛ 79343.csv'
df = pd.read_excel('excel/Тарифы по прокату - 8.xlsx', sheet_name='ПЛ 79343')
df.to_csv('csv/' + clean_file, index=False)

input = open('csv/' + clean_file, 'r')
output = open('csv/clean_' + clean_file, 'w')
writer = csv.writer(output)

for row in csv.reader(input):
    writer.writerow(row[:4])

input.close()
output.close()
os.remove('csv/' + clean_file)
print(f'[INFO] Garbage cleaned')
