import random
import json

dictData = [{"id": 111111,
             "departureDate": "01.03.2020",
             "noLimits": False,
             "stationCode": 521001,
             "clientCode": 6621,
             "wagonType": "ФТГ",
             "wagonCapacity": 72,
             "attachType": "RM COIL-3",
             "attachNum": 3
             },

            {"id": 222222,
             "departureDate": "02.02.2020",
             "noLimits": False,
             "stationCode": 521002,
             "clientCode": 6621,
             "wagonType": "ФТГ",
             "wagonCapacity": 72,
             "attachType": "RM COIL-3",
             "attachNum": 3
             }
            ]
jsonData = json.dumps(dictData, ensure_ascii=False)

# print(jsonData)
with open("json/data.json", "w") as file:
    file.write(jsonData)


# testData = json.loads(jsonData)
# print(testData[0]["id"])